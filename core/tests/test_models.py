from django.test import TestCase
from django.contrib.auth import get_user_model

class ModelTest(TestCase):

    def test_create_user_with_email_successful(self):
        """Prueba para crear usuario exitosamente
            Try to create user successful"""
        email = 'cleon@corrosioncic.com'
        password = '123456'
        user = get_user_model().objects.create_user(
            email = email,
            password = password
        )

        self.assertEqual(user.email,email)
        self.assertTrue(user.check_password(password))

    def test_new_user_email_normalized(self):
        """Testea email para nuevo email normalizado"""
        email = 'test@CORROSIONCIC.com'
        user = get_user_model().objects.create_user(
            email,
            '123456'
        )

        self.assertEqual(user.email, email.lower())

    def test_new_user_invalid_email(self):
        """email invalido de nuevo usuario"""
        with self.assertRaises(ValueError):
         user = get_user_model().objects.create_user(None,'123456')

    def test_create_new_superuser(self):
        """Crear un super usuario valido"""
        email = 'cleon@corrosioncic.com'
        password = '123456'
        user = get_user_model().objects.create_superuser(
            email = email,
            password = password
        )

        self.assertTrue(user.is_superuser)
        self.assertTrue(user.is_staff)
